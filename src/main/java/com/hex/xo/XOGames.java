package com.hex.xo;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Code.Addict
 */
import java.util.Scanner;    

public class XOGames {
     /** HEX@ Keyboard input */
    static Scanner get = new Scanner(System.in);

    /** HEX@ Short print */
    static void writeOut(String self) {
        System.out.println(self);
    }

    /** HEX@ Show Board */
    static void lookUp(int[] board) {
        String strResult = "\n  1 2 3";
        boolean isNewLine = false;
        int nowRow = 1;

        for (int i = 0; i < board.length; i++) {

            isNewLine = (i % 3 == 0) ? true : false;

            if (isNewLine) {
                strResult += "\n" + nowRow + " ";
                nowRow++;
            }

            strResult += (board[i] == 1 ? 'X' : board[i] == 2 ? 'Y' : '-') + " ";
        }
        
        writeOut(strResult);
    }
    
    /** HEX@ Get Winner */
    static int getWinner(int[] board, int round, int playerTurn) {
        // 0, 0, 0,
        // 0, 0, 0,
        // 0, 0, 0
        if (round < 3) {
            return 0;
        }
                
        // To do for get A real winner callback( true ); 
        return  (board[0] == playerTurn && board[1] == playerTurn && board[2] == playerTurn) ? playerTurn :
                (board[3] == playerTurn && board[4] == playerTurn && board[5] == playerTurn) ? playerTurn :
                (board[6] == playerTurn && board[7] == playerTurn && board[8] == playerTurn) ? playerTurn :
                (board[0] == playerTurn && board[3] == playerTurn && board[6] == playerTurn) ? playerTurn :
                (board[1] == playerTurn && board[4] == playerTurn && board[7] == playerTurn) ? playerTurn :
                (board[2] == playerTurn && board[5] == playerTurn && board[8] == playerTurn) ? playerTurn :
                (board[0] == playerTurn && board[4] == playerTurn && board[8] == playerTurn) ? playerTurn :
                (board[2] == playerTurn && board[4] == playerTurn && board[6] == playerTurn) ? playerTurn : round > 9 ? 3 : 0;

    }
    
    /** HEX If player try to make the game bug or what ever this function ganne input every condition for check is it should be successfully ?  */
    // R1=(1*3-1) / 2 - 2 / 0
    // R2=(2*3-1) / 5 - 2 / 3
    // R3=(3*3-1) / 8 - 2 / 6 
    // Rn > 8 / error out of table
    // Rn < 8 and xoBoard[Rn - 2 + C]
    static int issueCheck(int[] board, int row, int column, boolean isXTurn) {

        int RowSelected = row * 3 - 1;
        if (RowSelected > 8 || RowSelected < 2 || column < 1 || column > 3) {
            writeOut("\n\n[ !Error ] OX Position input wrong...\n");
            return 0;
        }

        int PosSecelect = (RowSelected - 2) + (column - 1);
        if (board[PosSecelect] != 0) {
            writeOut("\n\n[ !Error ] Position already assiged before...\n");
            return 0;
        }

        // Assigned OX in to TABLE .
        board[PosSecelect] = isXTurn ? 1 : 2;
        return 1;
    }

    static int insertBoard(int[] board, int round) {
        boolean isXTurn = round % 2 == 1;

        writeOut((isXTurn ? 'X' : 'Y') + " turn");
        writeOut("Please input Row Col: ");

        int row = get.nextInt(), column = get.nextInt();
        return issueCheck(board, row, column, isXTurn);
    }

    /*      HEX.New XO Games XD
    *           0 Meaning -> '-'
    *           1 Meaning -> 'X'
    *           2 Meaning -> 'Y'
    */
    public static void main(String[] argvs) {
        
        int[] boardA = new int[] {
            0, 0, 0,
            0, 0, 0,
            0, 0, 0
        };
        int roundA = 1;
        int cachedroundA = -1;
        int winnerA = -1;
        int playerTurnA = -1;

        writeOut("Welcome to OX Game");
        do {

            playerTurnA = roundA % 2 == 1 ? 1 : 2;

            if (cachedroundA != roundA) {
                cachedroundA = roundA;
                lookUp(boardA);
            }

            roundA += insertBoard(boardA, roundA);
            winnerA = getWinner(boardA, roundA, playerTurnA);
        } while (winnerA == 0);
        
        lookUp(boardA);

        writeOut(winnerA == 3 ? "Draw Game :D" : "Player " + (winnerA % 2 == 1 ? 'X' : 'Y') + " Win....");
        writeOut("Bye bye....");
    }
}
